FROM node:lts as builder

WORKDIR /var/app/

COPY package.json tsconfig.json yarn.lock ./
RUN yarn install

COPY src/ ./
RUN yarn build

FROM node:lts as target

WORKDIR /var/app/

COPY package.json yarn.lock ./
RUN yarn install --production

COPY --from=builder /var/app/dist/ ./dist/

CMD ["yarn", "start"]
